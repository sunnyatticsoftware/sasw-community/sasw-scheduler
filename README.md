# sasw-scheduler

Generic and domain-agnostic scheduler that can persist, queue and run jobs.

## Features
- It uses .NET 5
- Hangfire read only dashboard available at /hangfire
- Swagger available at /swagger
- Built on top of Hangfire open source library
- Sql Server persistence and can also be configured to use in-memory persistance
- Configurable per environment by using appsettings.
- IoCC with Microsoft Dependency Injection
- Http job executor support to send an http request with the scheduled job's payload to a given URL using the given headers and http verb.
- 10 automated retries if remote endpoint does not answer with 2xx response. After that, the job will be moved to failed jobs.
- Exposes secured http API to schedule jobs (see section How to use).
- Supports both Basic authentication and Api Key authentication (see section How to use).
- Supports placeholders in base url and relative url (see section How to use).

## How to use

### Schedule a job
To schedule a job, POST a message to the `/WebApi/jobs` url with api key authentication, specifying the date and time when the job will run, the payload the job will post, and the client Id
that defines the client endpoint and authorization header. 

Example to schedule a job:

```
POST https://localhost:5000/WebApi/jobs

x-api-key: <api_key_value>
Content-Type: application/json
Body:
{
    "payload": "{\"This is the json payload that will be posted to the given client on the given date\"}",
    "executeOn": "2020-11-04T15:00:00.000000Z",
    "clientId": "Sample"
}
```
Instead of using api key header, basic authentication is also supported by providing a basic authorization header such as `Authorization: Basic <username:password_base64_formatted>`.

The specified client Id must exist in `appsettings.json`. It will be read at the time the job is processed, and the specified url and headers will be used to send the http request 
with the given payload using the specified http verb (e.g: "Post", "Put", "Patch").
Example configured at `appsettings.json` for a specific client Id:
```
"Clients": {
    "Sample": {
      "BaseUrl": "https://sample.free.beeceptor.com",
      "RelativeUrl": "my/api/path",
      "Verb": "Post",
      "Headers": [
        {
          "Key": "Authorization",
          "Value": "Basic Zm9vOmJhcg=="
        },
        {
          "Key": "x-custom-header",
          "Value": "anotherValue"
        },
      ]
    }
  }
```

### Use placeholders
On some occasions, the url for a specific client has a dynamic part that depends on a specific job. It's possible to add placeholders to base or relative url
and provide some url parameters when scheduling a job.
Example:
```
"Clients": {
    "Sample": {
      "BaseUrl": "https://{{beeceptor_name}}.free.beeceptor.com",
      "RelativeUrl": "{{relative_url}}",
      "Verb": "Post",
      "Headers": [
        {
          "Key": "Authorization",
          "Value": "Basic Zm9vOmJhcg=="
        },
        {
          "Key": "x-custom-header",
          "Value": "anotherValue"
        },
      ]
    }
  }
```

When scheduling a job, you can provide the values for those placeholders.
Example:
```
POST https://localhost:5000/WebApi/jobs

x-api-key: <api_key_value>
Content-Type: application/json
Body:
{
    "payload": "{\"This is the json payload that will be posted to the given client on the given date\"}",
    "executeOn": "2020-11-04T15:00:00.000000Z",
    "clientId": "Sample",
    "urlParameters": [
        {
          "key": "beeceptor_name",
          "value": "sample"
        },
        {
          "key": "relative_url",
          "value": "my/api/path" 
        }
    ] 
}
```
