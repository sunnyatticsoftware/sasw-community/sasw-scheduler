﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace Sasw.Scheduler.WebApi.FunctionalTests.TestSupport.Extensions
{
    public static class HttpClientExtensions
    {
        public static HttpClient AddBasicAuthentication(this HttpClient httpClient, string username, string password)
        {
            var seed = $"{username}:{password}";
            var token = ToBase64(seed);
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
            return httpClient;
        }

        public static HttpClient AddApiKeyAuthentication(this HttpClient httpClient, string apiKeyHeader, string value)
        {
            httpClient.DefaultRequestHeaders.Add(apiKeyHeader, value);
            return httpClient;
        }

        private static string ToBase64(string text)
        {
            var textBytes = Encoding.UTF8.GetBytes(text);
            var textBase64 = Convert.ToBase64String(textBytes);
            return textBase64;
        }
    }
}
