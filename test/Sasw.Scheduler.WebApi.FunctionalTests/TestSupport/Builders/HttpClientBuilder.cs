﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Moq;

namespace Sasw.Scheduler.WebApi.FunctionalTests.TestSupport.Builders
{
    public class HttpClientBuilder
    {
        private HttpResponseMessage _httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
        private RequestInterceptor _requestInterceptor = new RequestInterceptor();

        public HttpClientBuilder WithResponse(HttpResponseMessage httpResponseMessage)
        {
            _httpResponseMessage = httpResponseMessage;
            return this;
        }

        public HttpClientBuilder WithRequestInterceptor(RequestInterceptor requestInterceptor)
        {
            _requestInterceptor = requestInterceptor;
            return this;
        }

        public HttpClient Build()
        {
            var httpMessageHandlerMock = new Mock<IMockHttpMessageHandler>();
            httpMessageHandlerMock
                .Setup(x => x.SendAsync(It.IsAny<HttpRequestMessage>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(_httpResponseMessage);

            var httpMessageHandler = new MockHttpMessageHandler(httpMessageHandlerMock.Object, _requestInterceptor);

            var httpClient = new HttpClient(httpMessageHandler);
            return httpClient;
        }
    }

    public interface IMockHttpMessageHandler
    {
        Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken);
    }

    public class RequestInterceptor
    {
        public string HttpMethod { get; set; }
        public string Url { get; set; } = string.Empty;
        public KeyValuePair<string, string>[] Headers { get; set; } = {};
        public string Content { get; set; } = string.Empty;
    }

    public class MockHttpMessageHandler
        : HttpMessageHandler
    {
        private readonly IMockHttpMessageHandler _mockHttpMessageHandler;
        private readonly RequestInterceptor _requestInterceptor;

        public MockHttpMessageHandler(
            IMockHttpMessageHandler mockHttpMessageHandler,
            RequestInterceptor requestInterceptor)
        {
            _mockHttpMessageHandler = mockHttpMessageHandler;
            _requestInterceptor = requestInterceptor;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            _requestInterceptor.Url = request.RequestUri.OriginalString;
            _requestInterceptor.HttpMethod = request.Method.ToString();
            _requestInterceptor.Headers = request.Headers.Select(h => new KeyValuePair<string, string>(h.Key, h.Value.First().ToString())).ToArray();
            _requestInterceptor.Content = request.Content.ReadAsStringAsync().GetAwaiter().GetResult();

            return _mockHttpMessageHandler.SendAsync(request, cancellationToken);
        }
    }
}
