﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Sasw.Scheduler.Application.Abstractions.EndpointClients;
using Sasw.Scheduler.Application.Abstractions.Jobs;
using Sasw.Scheduler.Application.Models;
using Sasw.Scheduler.WebApi.FunctionalTests.TestSupport;
using Sasw.Scheduler.WebApi.FunctionalTests.TestSupport.Builders;
using Xunit;

namespace Sasw.Scheduler.WebApi.FunctionalTests.UseCases.ExecuteJob
{
    public static class ExecuteJobTests
    {
        public class Given_Job_Scheduled_When_Executing
            : FunctionalTest
        {
            private IJobExecutor _jobExecutor;
            private Job _job = null!;
            private Exception _exception = null!;
            private RequestInterceptor _requestInterceptor = null!;
            private string _expectedUrl = null!;
            private string _expectedContent = null!;
            private KeyValuePair<string, string> _expectedAuthorizationHeader = default;
            private KeyValuePair<string, string> _expectedApiKeyHeader = default;

            protected override void ConfigureTestServices(IServiceCollection services)
            {
                _requestInterceptor = new RequestInterceptor();
                var httpClient =
                    new HttpClientBuilder()
                        .WithRequestInterceptor(_requestInterceptor)
                        .Build();

                var httpClientFactoryMock = new Mock<IHttpClientFactory>();
                httpClientFactoryMock
                    .Setup(x => x.Create())
                    .Returns(httpClient);

                services.AddSingleton(sp => httpClientFactoryMock.Object);
            }

            protected override Task Given()
            {
                _jobExecutor = GetService<IJobExecutor>();
                _job =
                    new Job
                    {
                        ClientId = "TestClientOne",
                        ExecuteOn = new DateTime(2020, 10, 30, 0, 0, 0, DateTimeKind.Utc),
                        Payload = "{\"foo\";\"bar\"}"
                    };

                _expectedUrl = "http://test.com/foo/bar";
                _expectedAuthorizationHeader = new KeyValuePair<string, string>("Authorization", "Basic Zm9vOmJhcg==");
                _expectedApiKeyHeader = new KeyValuePair<string, string>("x-api-key", "ThisIsAnExampleForApiKey");
                _expectedContent = _job.Payload;
                return Task.CompletedTask;
            }

            protected override async Task When()
            {
                try
                {
                    await _jobExecutor.Execute(_job);
                }
                catch (Exception exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Not_Throw_Any_Exception()
            {
                _exception.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Send_A_Post_Request()
            {
                _requestInterceptor.HttpMethod.Should().Be("POST");
            }

            [Fact]
            public void Then_It_Should_Post_It_To_Expected_Client_Url()
            {
                _requestInterceptor.Url.Should().Be(_expectedUrl);
            }

            [Fact]
            public void Then_It_Should_Contain_Expected_Headers()
            {
                _requestInterceptor.Headers.Should().Contain(_expectedAuthorizationHeader);
                _requestInterceptor.Headers.Should().Contain(_expectedApiKeyHeader);
            }

            [Fact]
            public void Then_It_Should_Post_The_Expected_Content()
            {
                _requestInterceptor.Content.Should().BeEquivalentTo(_expectedContent);
            }
        }

        public class Given_Job_With_Url_Parameters_Scheduled_When_Executing
            : FunctionalTest
        {
            private IJobExecutor _jobExecutor;
            private Job _job = null!;
            private Exception _exception = null!;
            private RequestInterceptor _requestInterceptor = null!;
            private string _expectedUrl = null!;
            private string _expectedContent = null!;
            private KeyValuePair<string, string> _expectedAuthorizationHeader = default;
            private KeyValuePair<string, string> _expectedApiKeyHeader = default;

            protected override void ConfigureTestServices(IServiceCollection services)
            {
                _requestInterceptor = new RequestInterceptor();
                var httpClient =
                    new HttpClientBuilder()
                        .WithRequestInterceptor(_requestInterceptor)
                        .Build();

                var httpClientFactoryMock = new Mock<IHttpClientFactory>();
                httpClientFactoryMock
                    .Setup(x => x.Create())
                    .Returns(httpClient);

                services.AddSingleton(sp => httpClientFactoryMock.Object);
            }

            protected override Task Given()
            {
                _jobExecutor = GetService<IJobExecutor>();
                _job =
                    new Job
                    {
                        ClientId = "TestClientTwo",
                        ExecuteOn = new DateTime(2020, 10, 30, 0, 0, 0, DateTimeKind.Utc),
                        Payload = "{\"foo\";\"bar\"}",
                        UrlParameters =
                            new[]
                            {
                                new UrlParameter
                                {
                                    Key = "port_number",
                                    Value = "8080"
                                },
                                new UrlParameter
                                {
                                    Key = "resource_id",
                                    Value = "123"
                                }
                            }
                    };

                _expectedUrl = "http://test.com:8080/foo/123/bar";
                _expectedAuthorizationHeader = new KeyValuePair<string, string>("Authorization", "Basic Zm9vOmJhcg==");
                _expectedApiKeyHeader = new KeyValuePair<string, string>("x-api-key", "ThisIsAnExampleForApiKey");
                _expectedContent = _job.Payload;
                return Task.CompletedTask;
            }

            protected override async Task When()
            {
                try
                {
                    await _jobExecutor.Execute(_job);
                }
                catch (Exception exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Not_Throw_Any_Exception()
            {
                _exception.Should().BeNull();
            }

            [Fact]
            public void Then_It_Should_Send_A_Post_Request()
            {
                _requestInterceptor.HttpMethod.Should().Be("PUT");
            }

            [Fact]
            public void Then_It_Should_Post_It_To_Expected_Client_Url()
            {
                _requestInterceptor.Url.Should().Be(_expectedUrl);
            }

            [Fact]
            public void Then_It_Should_Contain_Expected_Headers()
            {
                _requestInterceptor.Headers.Should().Contain(_expectedAuthorizationHeader);
                _requestInterceptor.Headers.Should().Contain(_expectedApiKeyHeader);
            }

            [Fact]
            public void Then_It_Should_Post_The_Expected_Content()
            {
                _requestInterceptor.Content.Should().BeEquivalentTo(_expectedContent);
            }
        }
    }
}