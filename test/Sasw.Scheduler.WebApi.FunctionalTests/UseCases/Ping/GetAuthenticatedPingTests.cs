﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Configuration;
using Sasw.Scheduler.WebApi.FunctionalTests.TestSupport;
using Sasw.Scheduler.WebApi.FunctionalTests.TestSupport.Extensions;
using Xunit;

namespace Sasw.Scheduler.WebApi.FunctionalTests.UseCases.Ping
{
    public static class GetAuthenticatedPingTests
    {
        public class Given_A_Ping_Endpoint_Protected_With_BasicOrApiKeyAuth_When_Getting
            : FunctionalTest
        {
            private string _url = null!;
            private HttpResponseMessage _result;

            protected override Task Given()
            {
                _url = "ping/authenticated";
                return Task.CompletedTask;
            }

            protected override async Task When()
            {
                _result = await HttpClient.GetAsync(_url);
            }

            [Fact]
            public void Then_It_Should_Return_401_Unauthorized()
            {
                _result.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
            }
        }

        public class Given_A_Ping_Endpoint_Protected_With_BasicOrApiKeyAuth_And_A_Request_With_Basic_Auth_Header_When_Getting
            : FunctionalTest
        {
            private string _url = null!;
            private HttpResponseMessage _result;

            protected override Task Given()
            {
                _url = "ping/authenticated";
                var username = Configuration.GetValue<string>(SettingsConstants.AuthKeys.BasicUsername);
                var password = Configuration.GetValue<string>(SettingsConstants.AuthKeys.BasicPassword);
                HttpClient.AddBasicAuthentication(username, password);
                return Task.CompletedTask;
            }

            protected override async Task When()
            {
                _result = await HttpClient.GetAsync(_url);
            }

            [Fact]
            public void Then_It_Should_Return_200_Ok()
            {
                _result.StatusCode.Should().Be(HttpStatusCode.OK);
            }
        }

        public class Given_A_Ping_Endpoint_Protected_With_BasicOrApiKeyAuth_And_A_Request_With_ApiKey_Header_When_Getting
            : FunctionalTest
        {
            private string _url = null!;
            private HttpResponseMessage _result;

            protected override Task Given()
            {
                _url = "ping/authenticated";
                var apiKeyHeader = Configuration.GetValue<string>(SettingsConstants.AuthKeys.ApiKeyHeader);
                var apiKeyValue = Configuration.GetValue<string>(SettingsConstants.AuthKeys.ApiKeyValue);
                HttpClient.AddApiKeyAuthentication(apiKeyHeader, apiKeyValue);
                return Task.CompletedTask;
            }

            protected override async Task When()
            {
                _result = await HttpClient.GetAsync(_url);
            }

            [Fact]
            public void Then_It_Should_Return_200_Ok()
            {
                _result.StatusCode.Should().Be(HttpStatusCode.OK);
            }
        }
    }
}