﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Sasw.Scheduler.WebApi.FunctionalTests.TestSupport;
using Xunit;

namespace Sasw.Scheduler.WebApi.FunctionalTests.UseCases.Ping
{
    public static class GetPingTests
    {
        public class Given_A_Ping_Endpoint_When_Getting
            : FunctionalTest
        {
            private string _url = null!;
            private HttpResponseMessage _result;

            protected override Task Given()
            {
                _url = "ping";
                return Task.CompletedTask;
            }

            protected override async Task When()
            {
                _result = await HttpClient.GetAsync(_url);
            }

            [Fact]
            public void Then_It_Should_Return_200_Ok()
            {
                _result.StatusCode.Should().Be(HttpStatusCode.OK);
            }
        }
    }
}
