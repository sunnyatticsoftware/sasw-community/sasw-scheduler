﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Configuration;
using Sasw.Scheduler.WebApi.FunctionalTests.TestSupport;
using Sasw.Scheduler.WebApi.FunctionalTests.TestSupport.Builders;
using Sasw.Scheduler.WebApi.FunctionalTests.TestSupport.Extensions;
using Xunit;

namespace Sasw.Scheduler.WebApi.FunctionalTests.UseCases.AddJob
{
    public static class AddJobTests
    {
        public class Given_An_Unauthenticated_Request_When_Posting
            : FunctionalTest
        {
            private string _url = null!;
            private HttpResponseMessage _result;
            private StringContent _content = null!;

            protected override Task Given()
            {
                _url = "jobs";
                _content = new StringContent("foo");
                return Task.CompletedTask;
            }

            protected override async Task When()
            {
                _result = await HttpClient.PostAsync(_url, _content);
            }

            [Fact]
            public void Then_It_Should_Return_401_Unauthorized()
            {
                _result.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
            }
        }

        public class Given_A_Job_And_An_ApiKey_Authenticated_Request_When_Posting
            : FunctionalTest
        {
            private string _url = null!;
            private HttpResponseMessage _result;
            private StringContent _content = null!;

            protected override Task Given()
            {
                _url = "jobs";
                var apiKeyHeader = Configuration.GetValue<string>(SettingsConstants.AuthKeys.ApiKeyHeader);
                var apiKeyValue = Configuration.GetValue<string>(SettingsConstants.AuthKeys.ApiKeyValue);
                HttpClient.AddApiKeyAuthentication(apiKeyHeader, apiKeyValue);

                var jsonStream =
                    new FileStreamBuilder()
                        .WithFileResourceName("simple_job.json")
                        .Build();

                var executeOn = DateTime.UtcNow.ToString("o");

                _content =
                    new StringContentBuilder()
                        .WithContent(jsonStream)
                        .WithPlaceholderReplacement("execute_on", executeOn)
                        .Build();
                return Task.CompletedTask;
            }
            protected override async Task When()
            {
                _result = await HttpClient.PostAsync(_url, _content);
            }

            [Fact]
            public void Then_It_Should_Return_200_Ok()
            {
                _result.StatusCode.Should().Be(HttpStatusCode.OK);
            }
        }

        public class Given_A_Job_And_A_Basic_Authenticated_Request_When_Posting
            : FunctionalTest
        {
            private string _url = null!;
            private HttpResponseMessage _result;
            private StringContent _content = null!;

            protected override Task Given()
            {
                _url = "jobs";
                var username = Configuration.GetValue<string>(SettingsConstants.AuthKeys.BasicUsername);
                var password = Configuration.GetValue<string>(SettingsConstants.AuthKeys.BasicPassword);
                HttpClient.AddBasicAuthentication(username, password);

                var jsonStream =
                    new FileStreamBuilder()
                        .WithFileResourceName("simple_job.json")
                        .Build();

                var executeOn = DateTime.UtcNow.ToString("o");

                _content =
                    new StringContentBuilder()
                        .WithContent(jsonStream)
                        .WithPlaceholderReplacement("execute_on", executeOn)
                        .Build();
                return Task.CompletedTask;
            }
            protected override async Task When()
            {
                _result = await HttpClient.PostAsync(_url, _content);
            }

            [Fact]
            public void Then_It_Should_Return_200_Ok()
            {
                _result.StatusCode.Should().Be(HttpStatusCode.OK);
            }
        }

        public class Given_A_Job_With_Url_Parameters_And_An_ApiKey_Authenticated_Request_When_Posting
            : FunctionalTest
        {
            private string _url = null!;
            private HttpResponseMessage _result;
            private StringContent _content = null!;

            protected override Task Given()
            {
                _url = "jobs";
                var apiKeyHeader = Configuration.GetValue<string>(SettingsConstants.AuthKeys.ApiKeyHeader);
                var apiKeyValue = Configuration.GetValue<string>(SettingsConstants.AuthKeys.ApiKeyValue);
                HttpClient.AddApiKeyAuthentication(apiKeyHeader, apiKeyValue);

                var jsonStream =
                    new FileStreamBuilder()
                        .WithFileResourceName("simple_job_with_url_parameters.json")
                        .Build();

                var executeOn = DateTime.UtcNow.ToString("o");

                _content =
                    new StringContentBuilder()
                        .WithContent(jsonStream)
                        .WithPlaceholderReplacement("execute_on", executeOn)
                        .Build();
                return Task.CompletedTask;
            }
            protected override async Task When()
            {
                _result = await HttpClient.PostAsync(_url, _content);
            }

            [Fact]
            public void Then_It_Should_Return_200_Ok()
            {
                _result.StatusCode.Should().Be(HttpStatusCode.OK);
            }
        }

        public class Given_A_Job_With_Url_Parameters_And_A_Basic_Authenticated_Request_When_Posting
            : FunctionalTest
        {
            private string _url = null!;
            private HttpResponseMessage _result;
            private StringContent _content = null!;

            protected override Task Given()
            {
                _url = "jobs";
                var username = Configuration.GetValue<string>(SettingsConstants.AuthKeys.BasicUsername);
                var password = Configuration.GetValue<string>(SettingsConstants.AuthKeys.BasicPassword);
                HttpClient.AddBasicAuthentication(username, password);

                var jsonStream =
                    new FileStreamBuilder()
                        .WithFileResourceName("simple_job_with_url_parameters.json")
                        .Build();

                var executeOn = DateTime.UtcNow.ToString("o");

                _content =
                    new StringContentBuilder()
                        .WithContent(jsonStream)
                        .WithPlaceholderReplacement("execute_on", executeOn)
                        .Build();
                return Task.CompletedTask;
            }
            protected override async Task When()
            {
                _result = await HttpClient.PostAsync(_url, _content);
            }

            [Fact]
            public void Then_It_Should_Return_200_Ok()
            {
                _result.StatusCode.Should().Be(HttpStatusCode.OK);
            }
        }
    }
}