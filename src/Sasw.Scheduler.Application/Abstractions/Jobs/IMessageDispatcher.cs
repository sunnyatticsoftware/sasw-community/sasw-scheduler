﻿using System.Threading.Tasks;
using Sasw.Scheduler.Application.Models;

namespace Sasw.Scheduler.Application.Abstractions.Jobs
{
    public interface IMessageDispatcher
    {
        Task Dispatch(ClientSettings clientSettings, string payload, UrlParameter[] urlParameters);
    }
}