﻿using System.Threading.Tasks;
using Sasw.Scheduler.Application.Models;

namespace Sasw.Scheduler.Application.Abstractions.Jobs
{
    public interface IJobExecutor
    {
        Task Execute(Job job);
    }
}