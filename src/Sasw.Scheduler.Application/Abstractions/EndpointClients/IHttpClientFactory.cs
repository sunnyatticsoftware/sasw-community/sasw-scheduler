﻿using System.Net.Http;

namespace Sasw.Scheduler.Application.Abstractions.EndpointClients
{
    public interface IHttpClientFactory
    {
        HttpClient Create();
    }
}