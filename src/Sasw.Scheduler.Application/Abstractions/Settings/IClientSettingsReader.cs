﻿using Sasw.Scheduler.Application.Models;

namespace Sasw.Scheduler.Application.Abstractions.Settings
{
    public interface IClientSettingsReader
    {
        ClientSettings GetClientSettings(string clientId);
    }
}