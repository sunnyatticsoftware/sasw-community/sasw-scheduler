﻿namespace Sasw.Scheduler.Application.Abstractions.Settings
{
    public interface ISettingsReader
    {
        T GetSection<T>(string key);
    }
}