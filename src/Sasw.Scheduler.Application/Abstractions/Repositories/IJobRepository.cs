﻿using Sasw.Scheduler.Application.Models;

namespace Sasw.Scheduler.Application.Abstractions.Repositories
{
    public interface IJobRepository
    {
        void AddJob(Job job);
    }
}