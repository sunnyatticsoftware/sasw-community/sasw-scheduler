﻿using System.Linq;
using System.Net.Http;

namespace Sasw.Scheduler.Application.Models
{
    public class ClientSettings
    {
        public string BaseUrl { get; set; } = string.Empty;
        public string RelativeUrl { get; set; } = string.Empty;
        public string Verb { get; set; } = string.Empty;
        public HttpMethod HttpMethod => new HttpMethod(Verb.ToUpperInvariant());
        public ClientHttpHeader[] Headers { get; set; } = {};
        
        public override string ToString()
        {
            var headers = string.Join(",",Headers.Select(h => h.ToString()));
            return $"BaseUrl: {BaseUrl}, RelativeUrl: {RelativeUrl}, Verb: {Verb}, Headers: {headers}";
        }
    }

    public class ClientHttpHeader
    {
        public string Key { get; set; } = string.Empty;
        public string Value { get; set; } = string.Empty;
        public override string ToString()
        {
            return $"{Key}: {Value}";
        }
    }
}
