﻿namespace Sasw.Scheduler.Application.Models
{
    public class UrlParameter
    {
        public string Key { get; set; } = string.Empty;
        public string Value { get; set; } = string.Empty;
    }
}
