﻿using System;

namespace Sasw.Scheduler.Application.Models
{
    public class Job
    {
        public DateTime ExecuteOn { get; set; } = DateTime.UtcNow;
        public string Payload { get; set; } = string.Empty;
        public string ClientId { get; set; } = string.Empty;
        public UrlParameter[] UrlParameters { get; set; } = { };
    }
}
