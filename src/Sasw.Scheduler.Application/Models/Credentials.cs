﻿namespace Sasw.Scheduler.Application.Models
{
    public class Credentials
    {
        public string Username { get; }
        public string Password { get; }

        public Credentials(string username, string password)
        {
            Username = username;
            Password = password;
        }

        public static Credentials Default => new Credentials(string.Empty, string.Empty);
    }
}
