﻿using System;

namespace Sasw.Scheduler.Application.Exceptions
{
    public class MessageDispatcherException
        : Exception
    {
        public MessageDispatcherException(string message)
            : base(message)
        {
        }
    }
}
