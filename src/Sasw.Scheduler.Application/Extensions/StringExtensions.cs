﻿using System.Linq;
using Sasw.Scheduler.Application.Models;

namespace Sasw.Scheduler.Application.Extensions
{
    public static class StringExtensions
    {
        public static string ReplacePlaceholders(this string text, UrlParameter[] urlParameters)
        {
            var result =
                urlParameters
                    .Aggregate(text, (current, urlParameter) => 
                        current.Replace("{{" + urlParameter.Key + "}}", urlParameter.Value));
            return result;
        }
    }
}
