﻿using System.Threading.Tasks;
using Sasw.Scheduler.Application.Abstractions.Jobs;
using Sasw.Scheduler.Application.Abstractions.Settings;
using Sasw.Scheduler.Application.Models;

namespace Sasw.Scheduler.Application.Services.Jobs
{
    public class JobExecutor
        : IJobExecutor
    {
        private readonly IClientSettingsReader _clientSettingsReader;
        private readonly IMessageDispatcher _messageDispatcher;
        
        public JobExecutor(
            IClientSettingsReader clientSettingsReader,
            IMessageDispatcher messageDispatcher)
        {
            _clientSettingsReader = clientSettingsReader;
            _messageDispatcher = messageDispatcher;
        }

        public async Task Execute(Job job)
        {
            var clientId = job.ClientId;
            var payload = job.Payload;
            var urlParameters = job.UrlParameters;

            var clientSettings = _clientSettingsReader.GetClientSettings(clientId);
            await _messageDispatcher.Dispatch(clientSettings, payload, urlParameters);
        }
    }
}
