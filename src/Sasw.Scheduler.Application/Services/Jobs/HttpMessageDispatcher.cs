﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Sasw.Scheduler.Application.Abstractions.EndpointClients;
using Sasw.Scheduler.Application.Abstractions.Jobs;
using Sasw.Scheduler.Application.Exceptions;
using Sasw.Scheduler.Application.Extensions;
using Sasw.Scheduler.Application.Models;

namespace Sasw.Scheduler.Application.Services.Jobs
{
    public class HttpMessageDispatcher
        : IMessageDispatcher
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public HttpMessageDispatcher(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task Dispatch(ClientSettings clientSettings, string payload, UrlParameter [] urlParameters)
        {
            var baseUrl = clientSettings.BaseUrl.ReplacePlaceholders(urlParameters);
            using var httpClient = GetHttpClient(baseUrl, clientSettings.Headers);

            var relativeUrl = clientSettings.RelativeUrl.ReplacePlaceholders(urlParameters);
            var content = GetHttpContent(payload);

            var httpRequestMessage =
                new HttpRequestMessage(clientSettings.HttpMethod, relativeUrl)
                {
                    Content = content
                };

            var result = await httpClient.SendAsync(httpRequestMessage, HttpCompletionOption.ResponseHeadersRead);
            if (!result.IsSuccessStatusCode)
            {
                var message = $"Could not post content {payload} with settings: {clientSettings}";
                throw new MessageDispatcherException(message);
            }
        }

        private HttpClient GetHttpClient(string baseUrl, ClientHttpHeader[] headers)
        {
            var httpClient = _httpClientFactory.Create();
            httpClient.BaseAddress = new Uri(baseUrl);

            foreach (var clientHttpHeader in headers)
            {
                httpClient.DefaultRequestHeaders.Add(clientHttpHeader.Key, clientHttpHeader.Value);
            }

            return httpClient;
        }

        private HttpContent GetHttpContent(string payload)
        {
            var content = new StringContent(payload, Encoding.UTF8, "application/json");
            return content;
        }
    }
}
