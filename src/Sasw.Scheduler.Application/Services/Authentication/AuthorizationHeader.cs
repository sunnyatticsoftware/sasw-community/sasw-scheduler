﻿using System;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using Sasw.Scheduler.Application.Models;

namespace Sasw.Scheduler.Application.Services.Authentication
{
    public static class AuthorizationHeader
    {
        public static Credentials GetBasicAuthenticationCredentials(string authHeader)
        {
            var authHeaderValue = AuthenticationHeaderValue.Parse(authHeader);
            if (authHeaderValue.Scheme.Equals(AuthenticationSchemes.Basic.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                var credentials =
                    Encoding.UTF8
                        .GetString(Convert.FromBase64String(authHeaderValue.Parameter ?? string.Empty))
                        .Split(new[] {':'}, 2);
                if (credentials.Length == 2)
                {
                    return new Credentials(credentials[0], credentials[1]);
                }
            }

            return Credentials.Default;
        }
    }
}
