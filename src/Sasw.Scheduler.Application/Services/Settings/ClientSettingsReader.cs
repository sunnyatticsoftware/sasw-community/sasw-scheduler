﻿using Sasw.Scheduler.Application.Abstractions.Settings;
using Sasw.Scheduler.Application.Models;

namespace Sasw.Scheduler.Application.Services.Settings
{
    public class ClientSettingsReader
        : IClientSettingsReader
    {
        private readonly ISettingsReader _settingsReader;

        public ClientSettingsReader(ISettingsReader settingsReader)
        {
            _settingsReader = settingsReader;
        }

        public ClientSettings GetClientSettings(string clientId)
        {
            var clientSettings = _settingsReader.GetSection<ClientSettings>($"Clients:{clientId}");
            return clientSettings;
        }
    }
}
