﻿using System.Net.Http;
using Sasw.Scheduler.Application.Abstractions.EndpointClients;

namespace Sasw.Scheduler.Application.Services.EndpointClients
{
    public class HttpClientFactory
        : IHttpClientFactory
    {
        public HttpClient Create()
        {
            var httpClient = new HttpClient();
            return httpClient;
        }
    }
}
