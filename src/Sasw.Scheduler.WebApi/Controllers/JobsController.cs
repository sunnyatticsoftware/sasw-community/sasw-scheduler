﻿using Microsoft.AspNetCore.Mvc;
using Sasw.Scheduler.Application.Abstractions.Repositories;
using Sasw.Scheduler.Application.Models;
using Sasw.Scheduler.WebApi.Authentication;

namespace Sasw.Scheduler.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class JobsController
        : ControllerBase
    {
        private readonly IJobRepository _jobRepository;

        public JobsController(IJobRepository jobRepository)
        {
            _jobRepository = jobRepository;
        }

        [BasicOrApiKeyAuth]
        [HttpPost]
        public IActionResult AddJob([FromBody]Job job)
        {
            _jobRepository.AddJob(job);
            return Ok();
        }
    }
}
