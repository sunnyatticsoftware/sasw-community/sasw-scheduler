﻿using Microsoft.AspNetCore.Mvc;
using Sasw.Scheduler.WebApi.Authentication;

namespace Sasw.Scheduler.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PingController
        : ControllerBase
    {
        [HttpGet]
        public IActionResult GetPing()
        {
            return Ok("Go to /hangfire to view dashboard");
        }
        [BasicOrApiKeyAuth]
        [HttpGet("authenticated")]
        public IActionResult GetAuthenticatedPing()
        {
            return Ok("Go to /hangfire to view dashboard");
        }
    }
}
