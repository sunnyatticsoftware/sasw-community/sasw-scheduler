﻿namespace Sasw.Scheduler.WebApi
{
    public static class SettingsConstants
    {
        public static class HangfireKeys
        {
            public const string IsInMemory = "Hangfire:IsInMemory";
            public const string ConnectionString = "Hangfire:ConnectionString";
        }

        public static class AuthKeys
        {
            public const string BasicUsername = "Auth:Basic:Username";
            public const string BasicPassword = "Auth:Basic:Password";
            public const string ApiKeyHeader = "Auth:ApiKey:Header";
            public const string ApiKeyValue = "Auth:ApiKey:Value";
        }
    }
}
