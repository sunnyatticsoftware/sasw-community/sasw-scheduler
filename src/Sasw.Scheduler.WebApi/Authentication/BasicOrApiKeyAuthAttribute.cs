﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace Sasw.Scheduler.WebApi.Authentication
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class BasicOrApiKeyAuthAttribute
        : TypeFilterAttribute
    {
        public BasicOrApiKeyAuthAttribute(string realm = "WebApi")
            : base(typeof(BasicOrApiKeyAuthFilter))
        {
            Arguments = new object[] { realm };
        }
    }
}
