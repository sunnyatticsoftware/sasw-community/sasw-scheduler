﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Sasw.Scheduler.WebApi.Authentication
{
    public abstract class BaseAuthenticationFilter
        : IAuthorizationFilter
    {
        private readonly string _realm;

        protected BaseAuthenticationFilter(string realm)
        {
            _realm = realm ?? throw new ArgumentNullException(nameof(realm));
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var configuration = context.HttpContext.RequestServices.GetRequiredService<IConfiguration>();
            var headers = context.HttpContext.Request.Headers;
            try
            {
                var isAuthenticated = IsAuthenticated(configuration, headers);
                if (isAuthenticated)
                {
                    return;
                }
            }
            catch (FormatException)
            {
                ReturnUnauthorizedResult(context);
            }
            
            ReturnUnauthorizedResult(context);
        }

        public abstract bool IsAuthenticated(IConfiguration configuration, IHeaderDictionary headers);

        private void ReturnUnauthorizedResult(AuthorizationFilterContext context)
        {
            // Return 401 and a basic authentication challenge (causes browser to show login dialog)
            context.HttpContext.Response.Headers["WWW-Authenticate"] = $"Basic realm=\"{_realm}\"";
            context.Result = new UnauthorizedResult();
        }
    }
}
