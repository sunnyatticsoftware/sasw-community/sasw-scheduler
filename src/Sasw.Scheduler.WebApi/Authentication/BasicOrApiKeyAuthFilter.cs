﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Sasw.Scheduler.Application.Services.Authentication;

namespace Sasw.Scheduler.WebApi.Authentication
{
    public class BasicOrApiKeyAuthFilter
        : BaseAuthenticationFilter
    {
        public BasicOrApiKeyAuthFilter(string realm)
            : base(realm)
        {
        }

        public override bool IsAuthenticated(IConfiguration configuration, IHeaderDictionary headers)
        {
            var isAuthenticated =
                IsApiKeyAuthenticated(configuration, headers) ||
                IsBasicAuthenticated(configuration, headers);
            return isAuthenticated;
        }

        private bool IsBasicAuthenticated(IConfiguration configuration, IHeaderDictionary headers)
        {
            var basicAuthenticationHeader = headers["Authorization"];
            var basicAuthenticationCredentials = AuthorizationHeader.GetBasicAuthenticationCredentials(basicAuthenticationHeader);
            var username = basicAuthenticationCredentials.Username;
            var password = basicAuthenticationCredentials.Password;
            var expectedUsername = configuration.GetValue<string>(SettingsConstants.AuthKeys.BasicUsername);
            var expectedPassword = configuration.GetValue<string>(SettingsConstants.AuthKeys.BasicPassword);
            var isBasicAuthenticated = username == expectedUsername && password == expectedPassword;
            return isBasicAuthenticated;
        }

        private bool IsApiKeyAuthenticated(IConfiguration configuration, IHeaderDictionary headers)
        {
            var apiKeyHeader = configuration.GetValue<string>(SettingsConstants.AuthKeys.ApiKeyHeader);
            var apiKeyValue = headers[apiKeyHeader];
            var expectedApiKeyValue = configuration.GetValue<string>(SettingsConstants.AuthKeys.ApiKeyValue);
            var isApiKeyAuthenticated = apiKeyValue == expectedApiKeyValue;
            return isApiKeyAuthenticated;
        }
    }
}
