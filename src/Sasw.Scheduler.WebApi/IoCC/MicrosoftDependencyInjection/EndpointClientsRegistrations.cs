﻿using Microsoft.Extensions.DependencyInjection;
using Sasw.Scheduler.Application.Abstractions.EndpointClients;
using Sasw.Scheduler.Application.Services.EndpointClients;

namespace Sasw.Scheduler.WebApi.IoCC.MicrosoftDependencyInjection
{
    public static class EndpointClientsRegistrations
    {
        public static IServiceCollection AddEndpointClients(this IServiceCollection services)
        {
            services.AddTransient<IHttpClientFactory, HttpClientFactory>();

            return services;
        }
    }
}
