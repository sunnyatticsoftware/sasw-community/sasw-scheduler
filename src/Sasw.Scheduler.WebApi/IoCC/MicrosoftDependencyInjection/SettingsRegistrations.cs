﻿using Microsoft.Extensions.DependencyInjection;
using Sasw.Scheduler.Application.Abstractions.Settings;
using Sasw.Scheduler.Application.Services.Settings;
using Sasw.Scheduler.WebApi.Settings;

namespace Sasw.Scheduler.WebApi.IoCC.MicrosoftDependencyInjection
{
    public static class SettingsRegistrations
    {
        public static IServiceCollection AddSchedulerSettings(this IServiceCollection services)
        {
            services.AddTransient<ISettingsReader, SettingsReader>();
            services.AddTransient<IClientSettingsReader, ClientSettingsReader>();

            return services;
        }
    }
}
