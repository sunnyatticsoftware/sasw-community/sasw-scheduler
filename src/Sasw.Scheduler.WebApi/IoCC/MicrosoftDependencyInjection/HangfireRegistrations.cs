﻿using System;
using Hangfire;
using Hangfire.MemoryStorage;
using Hangfire.SqlServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Sasw.Scheduler.Application.Abstractions.Jobs;
using Sasw.Scheduler.Application.Abstractions.Repositories;
using Sasw.Scheduler.Application.Services.Jobs;
using Sasw.Scheduler.Infra.Hangfire;

namespace Sasw.Scheduler.WebApi.IoCC.MicrosoftDependencyInjection
{
    public static class HangfireRegistrations
    {
        public static IServiceCollection AddSchedulerHangfire(this IServiceCollection services)
        {
            services
                .AddHangfire(
                    (serviceProvider, config) =>
                    {
                        config
                            .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                            .UseSimpleAssemblyNameTypeSerializer()
                            .UseRecommendedSerializerSettings();
                        var configuration = serviceProvider.GetService<IConfiguration>();
                        var isInMemory = configuration.GetValue<bool>(SettingsConstants.HangfireKeys.IsInMemory);
                        var foo = configuration.GetValue<string>("Clients:TestClientTwo:BaseUrl");
                        if (isInMemory)
                        {
                            config.UseMemoryStorage();
                        }
                        else
                        {
                            var connectionString = configuration.GetValue<string>(SettingsConstants.HangfireKeys.ConnectionString);
                            var sqlServerStorageOptions =
                                new SqlServerStorageOptions
                                {
                                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                                    QueuePollInterval = TimeSpan.Zero,
                                    UseRecommendedIsolationLevel = true,
                                    DisableGlobalLocks = true
                                };
                            try
                            {
                                config.UseSqlServerStorage(connectionString, sqlServerStorageOptions);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                                throw;
                            }
                        }
                    });

            services.AddTransient<IJobRepository, JobRepository>();
            services.AddTransient<IJobExecutor, JobExecutor>();
            services.AddTransient<IMessageDispatcher, HttpMessageDispatcher>();

            // Add it as a IHostedService when no memory storage
            services.AddHangfireServer();

            return services;
        }
    }
}
