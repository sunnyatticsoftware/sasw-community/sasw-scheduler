﻿using Microsoft.Extensions.Configuration;
using Sasw.Scheduler.Application.Abstractions.Settings;

namespace Sasw.Scheduler.WebApi.Settings
{
    public class SettingsReader
        : ISettingsReader
    {
        private readonly IConfiguration _configuration;

        public SettingsReader(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public T GetSection<T>(string key)
        {
            var section = _configuration.GetSection(key).Get<T>();
            return section;
        }
    }
}
