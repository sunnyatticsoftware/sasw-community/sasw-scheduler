﻿using Hangfire.Dashboard;

namespace Sasw.Scheduler.Infra.Hangfire.Authorization
{
    public class DashboardAuthorizationFilter
        : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            return true;
        }
    }
}
