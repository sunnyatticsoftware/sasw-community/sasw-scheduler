using Hangfire;
using Microsoft.AspNetCore.Builder;
using Sasw.Scheduler.Infra.Hangfire.Authorization;

namespace Sasw.Scheduler.Infra.Hangfire.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static void UseHangfireAuthenticatedDashboard(this IApplicationBuilder app)
        {
            var dashboardOptions =
                new DashboardOptions
                {
                    Authorization = new[] {new DashboardAuthorizationFilter()},
                    IsReadOnlyFunc = ctx => true,
                    
                    IgnoreAntiforgeryToken = true,
                    DashboardTitle = "Sasw Scheduler",
                    PrefixPath = null // Useful if there's some path rewrite in server
                };

            //NOTE: app.UseHangfireServer(); is not needed when registering it as hosted service
            app.UseHangfireDashboard("/hangfire", dashboardOptions);
        }
    }
}
