﻿using Hangfire;
using Sasw.Scheduler.Application.Abstractions.Jobs;
using Sasw.Scheduler.Application.Abstractions.Repositories;
using Sasw.Scheduler.Application.Models;

namespace Sasw.Scheduler.Infra.Hangfire
{
    public class JobRepository
        : IJobRepository
    {
        public void AddJob(Job job)
        {
            BackgroundJob.Schedule<IJobExecutor>(j => j.Execute(job), job.ExecuteOn);
        }
    }
}
